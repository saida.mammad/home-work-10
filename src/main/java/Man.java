import java.util.Map;

public final class Man extends Human {
    public Man() {
    }

    public Man(String name, String surname) {
        super(name, surname);
    }

    public Man(String name, String surname, String birthdayDate, int iq, Map<String, String> weekNotes) {
        super(name, surname, birthdayDate, iq, weekNotes);
    }

    public Man(String vasif, String salimov, String s, int i) {
    }

    void repairCar() {
        System.out.println("чинить авто");
    }

    @Override
    public void greetPet(Pet pet) {
        System.out.println("Привет, " + pet.getSpecies());
    }

}
