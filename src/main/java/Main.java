import java.util.HashSet;
import java.util.Set;
import java.text.SimpleDateFormat;
import java.util.*;

public class Main {
    public static void main(String[] args) {

        Human child1 = new Man( "Nazrin", "Salimli" );
        child1.setBirthDate( "21.04.2012" );
        Human child2 = new Man( "Kamil", "Salimov" );
        Human woman = new Woman( "Saida", "Salimova" );
        Human man = new Man( "Mammad", "Salimov" );
        Family family = new Family( woman, man );
        family.addChild( child1 );

        Human woman1 = new Woman( "Sitara", "Mammadova" );
        Human man1 = new Man( "Farman", "Mammadov" );
        Family family1 = new Family( woman1, man1 );
        family1.addChild( child2 );

        FamilyDao familyDao = new CollectionFamilyDao();
        familyDao.saveFamily( family );
        familyDao.saveFamily( family1 );


        FamilyService familyService = new FamilyService( familyDao );
        FamilyController familyController = new FamilyController( familyService );


        System.out.println( familyController.countFamiliesWithMemberNumber( 3 ) );

        System.out.println( family.getChildren().get( 0 ).getBirthDate() );
        System.out.println( family.getChildren().get( 0 ).describeAge() );

        Human human = new Man( "Vasif", "Salimov", "13.02.1956", 10 );

        System.out.println( human );
        System.out.println( child1.toString() );


    }
}

