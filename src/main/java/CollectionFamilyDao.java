import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDao implements FamilyDao {

    private List<Family> familyList = new ArrayList<>();

    @Override
    public List<Family> getAllFamilies(){
        return familyList;
    }

    @Override
    public Family getFamilyByIndex(int index){
        return index < 0 || index > familyList.size()-1 ?  null : familyList.get(index);
    }

    @Override

    public boolean deleteFamily (int index){
        if (index < 0 || index > familyList.size()-1) {
            return false;
        } else familyList.remove(index);
        return true;

    }

    @Override
    public boolean deleteFamily(Family family){
        return familyList.remove(family);
    }

    @Override

    public void saveFamily (Family family){
         familyList.add(family);

    }
}
