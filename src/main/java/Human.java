
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.Objects;

abstract class Human {
    private String name;
    private String surname;
    private long birthDate;
    private int iq;
    private Map<String, String> weekNotes;
    private Family family;

    public Human() {
    }

    public Human(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public Human(String name, String surname, String birthDate, int iq, Map<String, String> weekNotes) {
        this.name = name;
        this.surname = surname;
        simpleDateFormat( birthDate );
        this.iq = iq;
        this.weekNotes = weekNotes;
    }

    public Human(String name, String surname, String birthDate, int iq) {
        this.name = name;
        this.surname = surname;
        simpleDateFormat( birthDate );
        this.iq = iq;
    }

    public String describeAge() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime( new Date( this.birthDate ) );
        LocalDate birthDate = LocalDate.of( calendar.get( Calendar.YEAR ), calendar.get( Calendar.MONTH ) + 1, calendar.get( Calendar.DAY_OF_MONTH ) );
        LocalDate now = LocalDate.now();
        Period period = Period.between( birthDate, now );
        return getName() + " " + getSurname() + ", тебе " + period.getYears() + " года, " + period.getMonths() + " месяцев, " + period.getDays() + " дней.";

    }

    public void simpleDateFormat(String s) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat( "dd.MM.yyyy" );
        Date date = null;
        try {
            date = (Date) simpleDateFormat.parse( s );
        } catch (ParseException e) {
            e.printStackTrace();
        }
        assert date != null;
        this.birthDate = date.getTime();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return birthDate == human.birthDate &&
                Objects.equals( name, human.name ) &&
                Objects.equals( surname, human.surname );
    }

    @Override
    public int hashCode() {
        return Objects.hash( name, surname, birthDate );
    }


    public abstract void greetPet(Pet pet);

    public void describePet(Pet pet) {
        System.out.println( "У меня есть " + pet.getSpecies() + ". Eму " + pet.getAge() + " года, он хитрый!" );
    }

    public String toString() {
        if (birthDate == 0 && iq == 0 && weekNotes == null) {
            return "Human{name=" + name +
                    ", surname=" + surname + "}";
        } else {
            return "Human{name=" + name +
                    ", surname=" + surname +
                    ", birthDate=" + getBirthDate() +
                    ", iq=" + iq +
                    ", schedule=" + weekNotes + "}";
        }

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setBirthDate(String string) {
        simpleDateFormat( string );
    }

    public String getBirthDate() {
        Date date = new Date( birthDate );
        SimpleDateFormat dt = new SimpleDateFormat( "dd/MM/yyyy" );
        return dt.format( date );

    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public int getIq() {
        return iq;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public void setWeekNotes(Map<String, String> weekNotes) {
        this.weekNotes = weekNotes;
    }

    public Map<String, String> getWeekNotes() {
        return weekNotes;
    }
}


