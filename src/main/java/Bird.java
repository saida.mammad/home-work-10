import java.util.Set;

public class Bird extends Pet {
    public Bird() {
        species = Species.BIRD;
    }

    public Bird(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
        species = Species.BIRD;
    }

    @Override
    public void respond() {
        System.out.println("Вовка дурак");
    }
}
